#!/home/p13/env/bin/bin/python
# -*- coding: utf-8 -*-

import os, sys, zipfile
from wsgiref.simple_server import make_server
from cgi import parse_qs, escape
import cgi
import Image
from tempfile import TemporaryFile

sys.path.insert(0, '/home/p13/dom/')

activate_this = '/home/p13/env/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

def application(environ, start_response):

   uri = environ["PATH_INFO"]

   form = cgi.FieldStorage(fp=environ['wsgi.input'], environ=environ)
   fileitem = form['file']
   item = os.path.basename(fileitem.filename)
   sciezka='web'+ uri + item
   if (item.lower().endswith('.png') or item.lower().endswith('.jpg') or item.lower().endswith('.jpeg') or item.lower().endswith('.gif') ) and not os.path.exists(sciezka):
        open(sciezka,'wb').write(fileitem.file.read())
        im = Image.open(sciezka)
        width = 150
        height = 150
        im = im.resize((width, height), Image.BILINEAR)
        im.save('thumb'+ uri + item)
   elif item.lower().endswith('.zip'):
        with TemporaryFile() as tmp:
            tmp.write(fileitem.file.read())

            with zipfile.ZipFile(tmp, 'r', zipfile.ZIP_DEFLATED) as z:
                z.extractall("web"+uri)
                for f in z.namelist():
                    if f.lower().endswith('.png') or f.lower().endswith('.jpg') or f.lower().endswith('.jpeg') or f.lower().endswith('.gif'):
                        if not os.path.exists(os.path.dirname("thumb"+uri+f)):
                            os.makedirs(os.path.dirname("thumb"+uri+f))
                        im = Image.open("web"+uri+f)
                        width = 150
                        height = 150
                        im = im.resize((width, height), Image.BILINEAR)
                        im.save('thumb'+uri+ f)
                    elif not os.path.isdir("web"+uri+f):
                        os.remove("web"+uri+f)



   status = "200 OK"
   type="text/html"
   html = "<html><head><META HTTP-EQUIV='refresh' CONTENT='0.00001;URL=http://194.29.175.240/~p13/g.wsgi"+uri+"'></head><body></body></html>\n"

   response_headers = [('Content-type', type),
                       ('Content-length', str(len(html)))]
   start_response(status, response_headers)
   return [html]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('194.29.175.240', 30083, application)
    srv.serve_forever()