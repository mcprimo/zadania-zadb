#!/home/p13/env/bin/bin/python
# -*- coding: utf-8 -*-

import os, sys, zipfile
from wsgiref.simple_server import make_server
from cgi import parse_qs, escape
import cgi
import tempfile
import zipfile

sys.path.insert(0, '/home/p13/dom/')

activate_this = '/home/p13/env/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

def zipdir(path, zip):
    for root, dirs, files in os.walk(path):
        for file in files:
            zip.write(os.path.join(root, file))

def application(environ, start_response):

   uri = environ["PATH_INFO"]
   uri=uri.replace("download.zip","")

   html=""

   with tempfile.SpooledTemporaryFile() as tmp:
        with zipfile.ZipFile(tmp, 'w', zipfile.ZIP_DEFLATED) as archive:
            zipdir('web'+uri, archive)

        # Reset file pointer
        tmp.seek(0)

        # Write file data to response
        html=tmp.read()

   status = "200 OK"
   type="'application/x-zip-compressed"

   response_headers = [('Content-type', type),
                       ('Content-length', str(len(html)))]
   start_response(status, response_headers)
   return [html]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('194.29.175.240', 30013, application)
    srv.serve_forever()