#!/home/p13/env/bin/bin/python
# -*- coding: utf-8 -*-

import os, sys
from wsgiref.simple_server import make_server
from cgi import parse_qs, escape

sys.path.insert(0, '/home/p13/dom/')

activate_this = '/home/p13/env/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

def application(environ, start_response):

   uri = environ["PATH_INFO"]

   d = parse_qs(environ['QUERY_STRING'])
   nazwa = d.get('nazwa', [''])[0]
   nazwa2="/home/p13/public_html/web"+uri+nazwa
   if not os.path.exists(nazwa2): os.makedirs(nazwa2)
   nazwa2="/home/p13/public_html/thumb"+uri+nazwa
   if not os.path.exists(nazwa2): os.makedirs(nazwa2)

   status = "200 OK"
   type="text/html"
   html = "<html><head><META HTTP-EQUIV='refresh' CONTENT='0.00001;URL=http://194.29.175.240/~p13/g.wsgi"+uri+"'></head><body></body></html>\n"

   response_headers = [('Content-type', type),
                       ('Content-length', str(len(html)))]
   start_response(status, response_headers)
   return [html]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('194.29.175.240', 30013, application)
    srv.serve_forever()