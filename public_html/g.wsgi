#!/home/p13/env/bin/bin/python
# -*- coding: utf-8 -*-

import os, sys

sys.path.insert(0, '/home/p13/dom/')

activate_this = '/home/p13/env/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

def application(environ, start_response):

    html="<html><body><div style='width:100%;height:100%'>"
    uri = environ["PATH_INFO"]
    if os.path.isdir('/home/p13/public_html/web'+uri):
        dirList = os.listdir('/home/p13/public_html/web'+uri) #jak nie ma folderu to exception
        if uri != '':
            uri = uri + '/'
        for item in dirList:
            name=environ["SCRIPT_NAME"]

            if item.lower().endswith('.png') or item.lower().endswith('.jpg') or item.lower().endswith('.jpeg') or item.lower().endswith('.gif'):
                html=html+'<a href="'+name + uri + item + '">'+"<div style='height:100px;width:150px;display: inline-block;padding-right:50px;padding-bottom:75px;'><img src='http://194.29.175.240/~p13/thumb/"+uri+item+"'><div style='color:black;text-decoration:none;width:100%;'><center>&nbsp;<center></div></div>"+ '</a>'
            else:
                html=html+'<a href="'+name + uri + item + '">'+"<div style='height:100px;width:150px;display: inline-block;padding-right:50px;padding-bottom:75px;'><img src='http://194.29.175.240/~p13/folder.png'><div style='color:black;text-decoration:none;width:100%;'><center>"+item+"<center></div></div>"+ '</a>'

        status = "200 OK"
        type="text/html"
        tekst='"Jak chcesz nazwac katalog?",""'
        tekst2='"http://194.29.175.240/~p13/dodajf.wsgi'+uri+'?nazwa="+the_name'
        html = html+"<div style='position:absolute;bottom:5px;'><b><a style='text-decoration:underline;color:black' onclick='var the_name=window.prompt("+tekst+");if(the_name) window.location ="+tekst2+"'>Dodaj katalog</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='color:black' href='http://194.29.175.240/~p13/dod.wsgi"+uri+"'>Dodaj pliki</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='color:black' href='http://194.29.175.240/~p13/zip.wsgi"+uri+"download.zip'>Sciagnij zip</a></b></div>"
        html = html+"</body></html>\n"


    elif os.path.isfile('/home/p13/public_html/web'+uri):
        type = 'octet/stream'

        if uri.lower().endswith('.png'):
                type='image/png'
        elif uri.lower().endswith('.jpg'):
                type='image/jpeg'
        elif uri.lower().endswith('.jpeg'):
                type='image/jpeg'
        elif uri.lower().endswith('.gif'):
                type='image/gif'

        html = open('/home/p13/public_html/web'+uri).read()
        status = "200 OK"
    else:
         html = 'ERROR 404 NOT FOUND'
         status = "404 Not Found"
         type="text/html"
         html = html+"</div></body></html>\n"

    response_headers = [('Content-type', type),
                         ('Content-length', str(len(html)))]
    start_response(status, response_headers)
    return [html]


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    srv = make_server('194.29.175.240', 30013, application)
    srv.serve_forever()